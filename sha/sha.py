import hashlib
import os
from time import time
from os import scandir, getcwd

ruta="/Users/drodriguez/proyectos/cinvestav/seg_informatica/data/"

def gen_sha(sha_type):
    global_st=time()
    BLOCKSIZE = 65536    
    files = getData()
    files.sort()
    for f in files:
        start_time = time()
        data=open(ruta+f, 'rb')
        sizefile = os.stat(ruta+f).st_size #return in bytes
        bits_sizefile=sizefile*8 #size in bits
        with data as afile:
            buf = afile.read(BLOCKSIZE)
            while len(buf) > 0:
                hasher.update(buf)
                buf = afile.read(BLOCKSIZE)
        elapsed_time = time() - start_time
        bps=bits_sizefile/elapsed_time
        print("{}, {}, {}, {}, {}, {} ".format(hasher.hexdigest(),elapsed_time,f,sizefile,bits_sizefile,bps))
    print("global_t: ",time()-global_st)

def getData():
    files= [arch.name for arch in scandir(ruta) if arch.is_file() ]
    if '.DS_Store' in files:
        files.remove('.DS_Store')
    return files

if __name__ == "__main__":
    # SHA_Types: sha1, sha224, sha384, sha256, sha512
    shatype='sha256'
    if shatype == 'sha1':
        hasher = hashlib.sha1()
    elif shatype == 'sha224':
        hasher = hashlib.sha224()
    elif shatype == 'sha256':
        hasher = hashlib.sha256()
    elif shatype == 'sha384':
        hasher = hashlib.sha384()
    elif shatype == 'sha512':
        hasher = hashlib.sha512()
    print("Tipo de SHA: {}".format(shatype))    
    gen_sha(hasher)