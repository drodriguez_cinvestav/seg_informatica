from AES_Cipher import *

if __name__ == "__main__":
    ruta = "/Users/drodriguez/Proyectos/Cinvestav_code/seg_informatica/lab2/"

    archivos = listdir(ruta+"data/")
    #print(archivos)
    if '.DS_Store' in archivos:
        archivos.remove('.DS_Store')
    print(archivos)

    aes_c = AES_Cipher(ruta)
    kdf_on = False

    key = b'A2H3K4N5B6K7I6O7L0G9M9K3N5B6M7Z9'
    time_gen_key = 0
    for archivo in archivos:
        if kdf_on:
            ahora = time()
            password = b'my super secret'
            salt = get_random_bytes(16)
            key = PBKDF2(password, salt, 16, count=1000000,
                         hmac_hash_module=SHA512)
            time_gen_key = time() - ahora

        print('\n\n************ Algoritmo AES: {} ***********'.format(archivo))
        print("KEY:{}, Tiempo:{}".format(key.hex(), time_gen_key))
        print('Cifrando: ##########')
        aes_c.cifrar_archivo(archivo, key)
        print('Descifrando: ##########')
        aes_c.decifrar_archivo(archivo, key)
        print('Check_Bits: ##########')

        #print("SHA_Original:\t{}".format(getsha256(ruta+"data/", archivo)))
        #print("SHA_Descifrado:\t{}".format(getsha256(ruta+'descifrado/','aes_'+archivo)))
