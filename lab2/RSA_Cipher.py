from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives.asymmetric import utils

class RSA_Cipher:

    #Crifrando
    def cifrado(self, public_key, message,sha):
        ciphertext = public_key.encrypt(
            message,
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA256()),
                algorithm=sha,
                label=None
            )
        )
        return ciphertext

    #Descifrando
    def descifrado(self, private_key, ciphertext,sha):
        plaintext = private_key.decrypt(
            ciphertext,
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA256()),
                algorithm=sha,
                label=None
            )
        )
        return plaintext
