from Crypto.Protocol.KDF import PBKDF2
from Crypto.Random import get_random_bytes
from os import listdir
from Certificate import *
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives.asymmetric import utils
from time import time
from AES_Cipher import *
from RSA_Cipher import *

def genPKC(longitud):
    private_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=longitud,
        backend=default_backend())
    public_key = private_key.public_key()
    return [private_key, public_key]

#Firmar archivos
def firmarData(private_key,archivo, sha):
    chosen_hash = sha
    hasher = hashes.Hash(chosen_hash, default_backend())

    with open("data/"+archivo, "rb") as f:
            for bloque in iter(lambda: f.read(4096), b""):
                hasher.update(bloque)

    digest = hasher.finalize()
    signature = private_key.sign(
        digest,
        padding.PSS(
            mgf=padding.MGF1(hashes.SHA256()),
            salt_length=padding.PSS.MAX_LENGTH
        ),
        utils.Prehashed(chosen_hash))
    return signature

#Vericar firma
def checkSingnature(public_key, archivo, signature, sha):
    chosen_hash = sha
    hasher = hashes.Hash(chosen_hash, default_backend())
    with open(archivo, "rb") as f:
            for bloque in iter(lambda: f.read(4096), b""):
                hasher.update(bloque)
    digest = hasher.finalize()
    try:
        public_key.verify(
            signature,
            digest,
            padding.PSS(
                mgf=padding.MGF1(hashes.SHA256()),
                salt_length=padding.PSS.MAX_LENGTH
            ),
            utils.Prehashed(chosen_hash)
        )
    except Exception as e:
        return False
    else:
        return True

def getData(ruta):
    archivos = listdir(ruta)
    #print(archivos)
    if '.DS_Store' in archivos:
        archivos.remove('.DS_Store')
    return archivos

if __name__ == "__main__":
    inicioT = time()
    #Seguridad de las llaves PKC
    configuracion = 1
    if configuracion==1:
        key_size_pkc = 3072 #7680 #15360
        key_size_skc = 16 #24 32
        sha = hashes.SHA256() 
    elif configuracion == 2:
        key_size_pkc = 7680 #15360
        key_size_skc = 24 #32
        sha = hashes.SHA384()
    else:
        key_size_pkc = 15360
        key_size_skc = 32
        sha = hashes.SHA512()
    print("Configuración: {}, PKC: {}, SKC: {}".format(configuracion,key_size_pkc,key_size_skc*8))


    #Bob genera su llaves PKC {k_pub,kpriv}
    print("Generando llaves de Bob")
    inicio = time()
    bob_kpriv, bob_kpub = genPKC(key_size_pkc)
    print("Tiempo: ", time()-inicio)

    #Alice genera su llaves PKC {k_pub,kpriv}
    print("Generando llaves de Alice")
    inicio = time()
    alice_kpriv, alice_kpub = genPKC(key_size_pkc)
    print("Tiempo: ", time()-inicio)
    
    #Se obtinen los archivos a cifrar
    archivos = getData("data/")
    #print(archivos)

    #Tarea 2 Bob firma los datos
    print("Bob firmando sus documentos")
    inicio = time()
    firmas=[]
    for archivo in archivos:
        firma = firmarData(bob_kpriv,archivo,sha)
        firmas.append(firma)
    print("Tiempo: ", time()-inicio)
    print("Cantidad de documentos firmados: ",len(firmas))
   

    #Bob Genera Certifica
    print("Bob crea un certificado")
    inicio = time()
    cert = Certificate(bob_kpriv,bob_kpub)
    bob_cert = cert.getCertificate()
    print("Tiempo: ", time()-inicio)
    
    #Bob genera una llave simetrica
    print("Bob genera la llave simetrica")
    inicio = time()
    password = b'my super secret'
    salt = get_random_bytes(key_size_skc)
    key_simetrica = PBKDF2(password, salt, key_size_skc, count=100000)
    print("Llave simetrica: ",key_simetrica.hex())
    print("Tiempo: ", time()-inicio)
    #key_simetrica = b'A2H3K4N5B6K7I6O7L0G9M9K3N5B6M7Z9'

    #Bob cifra los datos
    print("Bob cifra sus documentos")
    inicio = time()
    ruta = "/Users/drodriguez/Proyectos/Cinvestav_code/seg_informatica/lab2/"
    aes_c = AES_Cipher(ruta)
    for archivo in archivos:
        aes_c.cifrar_archivo(archivo,key_simetrica)
    print("Tiempo: ", time()-inicio)
    print("Documentos cifrados")

    #Bob construye el sobre digital para compartir la llave simetrica
    print("Bob construye el sobre digital con la llave publica de Alice")
    inicio = time()
    rsa_c = RSA_Cipher()
    sobre = rsa_c.cifrado(alice_kpub,key_simetrica,sha)
    print("Tiempo: ", time()-inicio)

    #Alice recibe el sobre y extrae la llave_simetrica
    print("Alice abre el sobre")
    inicio = time()
    key_simetrica_des = rsa_c.descifrado(alice_kpriv,sobre,sha)
    print("Llave simetrica descifrada: ",key_simetrica_des.hex())
    print("Tiempo: ", time()-inicio)

    #Alice descifra los archivos
    print("Descifrando los archivos")
    inicio = time()
    for archivo in archivos:
        aes_c.decifrar_archivo(archivo, key_simetrica_des)
    print("Tiempo: ", time()-inicio)
    print("Documentos cifrados")

    #Alice verifica la validez del certificado
    print("Verificando certificado")
    inicio = time()
    check_cert = cert.validateCertificate(bob_cert, bob_kpub)
    print("Tiempo: ", time()-inicio)
    print("Certificado valido: ", check_cert)

    #Alice revisa las firmas
    print("Checking signatures")
    inicio = time()
    for i in range(len(archivos)):
        firma = firmas[i]
        archivo = archivos[i]
        singature_ok= checkSingnature(bob_kpub, "descifrado/aes_" + archivo, firma, sha)
        print("Archivo: {} -> Firma: {}".format(archivo,singature_ok))
    print("Tiempo: ", time()-inicio)
    print("Tiempo Total: ",time()-inicioT)

