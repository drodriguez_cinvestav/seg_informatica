from Crypto import Random
from Crypto.Cipher import AES
###KDF###
from Crypto.Protocol.KDF import PBKDF2
from Crypto.Hash import SHA512
from Crypto.Random import get_random_bytes
###KDF###
from time import time
from os import listdir
import hashlib
import os

class AES_Cipher:

    ruta=None

    def __init__(self,ruta):
        super().__init__()
        self.ruta=ruta
    

    def pad(self,s):
        return s + b"\1" * (AES.block_size - len(s) % AES.block_size)

    def cifrar(self,message, key):
        message = self.pad(message)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(key, AES.MODE_CBC, iv)
        return iv + cipher.encrypt(message)

    def decifrar(self, ciphertext, key):
        iv = ciphertext[:AES.block_size]
        cipher = AES.new(key, AES.MODE_CBC, iv)
        texto_plano = cipher.decrypt(ciphertext[AES.block_size:])
        return texto_plano.rstrip(b"\1")

    def cifrar_archivo(self, archivo, key):
        ahora = time()
        with open(self.ruta+"data/"+archivo, 'rb') as an:
            texto_plano = an.read()
        enc = self.cifrar(texto_plano, key)
        with open(self.ruta+'cifrado/'+archivo+'.aes', 'wb') as an:
            an.write(enc)

        tiempo_empleado = time() - ahora
        tam_original = os.path.getsize(self.ruta+"data/"+archivo)
        tam_cifrado = os.path.getsize(self.ruta+'cifrado/'+archivo+'.aes')
        bps_cifrado = tam_original*8/tiempo_empleado
        print("{} ({} bytes) -> {} ({} bytes), {} segundos, {} bits/seg".format(archivo,tam_original,archivo+".aes",tam_cifrado,tiempo_empleado,bps_cifrado))

    def decifrar_archivo(self, archivo, key):
        ahora = time()   
        with open(self.ruta+'cifrado/'+archivo+'.aes', 'rb') as ac:
            texto_cifrado = ac.read()
        dec = self.decifrar(texto_cifrado, key)

        with open(self.ruta+'descifrado/aes_'+archivo, 'wb') as ac:
            ac.write(dec)

        tiempo_empleado = time() - ahora
        tam_cifrado = os.path.getsize(self.ruta+'cifrado/'+archivo+'.aes')
        tam_descifrado = os.path.getsize(self.ruta+'descifrado/aes_'+archivo)
        bps_descifrado = tam_cifrado*8/tiempo_empleado
        print("{} ({} bytes) -> {} ({} bytes), {} segundos, {} bits/seg".format(archivo+'.aes',tam_cifrado,'aes_'+archivo,tam_descifrado,tiempo_empleado,bps_descifrado))


