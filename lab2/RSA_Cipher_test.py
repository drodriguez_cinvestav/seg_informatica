from RSA_Cipher import *

if __name__ == "__main__":
    #Generación de llaves
    random_generator = Crypto.Random.new().read
    private_key = RSA.generate(1024, random_generator)
    public_key = private_key.publickey()

    #Mensaje
    message = 'Hola mundo, soy un mensaje en texto plano, todo el mundo puede leerme.'
    print("Mensaje original: ", message)
    message = message.encode()
