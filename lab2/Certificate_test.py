from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa

from Certificate import *

private_key = rsa.generate_private_key(
    public_exponent=65537,
    key_size=2048,
    backend=default_backend()
    )

public_key = private_key.public_key()

cert = Certificate(private_key, public_key)
bob_cert = cert.getCertificate()
#print(bob_cert)

print(cert.validateCertificate(bob_cert,public_key))