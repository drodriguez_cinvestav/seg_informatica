import java.math.*;
import java.util.*;
import java.security.*;
import java.io.*;

public class Gammal{
    public static Random sc = new SecureRandom();
    
    public void cifrar(){

    }

    public static void main(String[] args) throws IOException{
        BigInteger p, b, c, secretKey;
        
        secretKey = new BigInteger("12345678901234567890");
        //
        // public key calculation
        //
        System.out.println("secretKey = " + secretKey);
        p = BigInteger.probablePrime(2048, sc);
        b = new BigInteger("3");
        c = b.modPow(secretKey, p);
        System.out.println("p = " + p);
        System.out.println("b = " + b);
        System.out.println("c = " + c);
        //
        // Encryption
        //
        BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Ingrese la cadena de texto: ");
        String s = f.readLine();
        BigInteger X = new BigInteger(s.getBytes());
        System.out.println("X:"+X);
        BigInteger r = new BigInteger(64, sc);
        BigInteger EC = X.multiply(c.modPow(r, p)).mod(p);
        BigInteger brmodp = b.modPow(r, p);
        System.out.println("Plaintext = " + X);
        System.out.println("r = " + r);
        System.out.println("EC = " + EC);
        System.out.println("b^r mod p = " + brmodp);
        //
        // Decryption
        //
        BigInteger crmodp = brmodp.modPow(secretKey, p);
        BigInteger d = crmodp.modInverse(p);
        BigInteger ad = d.multiply(EC).mod(p);
        System.out.println("\n\nc^r mod p = " + crmodp);
        System.out.println("d = " + d);
        System.out.println("Alice decodes: " + ad);
        String mesaje_des=new String(ad.toByteArray());
        System.out.println("Alice decodes: " + mesaje_des);
    }
}