/*
 * RSA.java
 *
 * Creado 24 de octubre de 2007, 12:02 PM
 *
 */


import java.math.BigInteger;
import java.util.*;
import java.io.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RSA {

   int tamPrimo;           //numero de bits de las claves privadas RSA
	
   BigInteger n, q, p;     // n = p*q, note que 'n' es mucho más grande que p y q (la suma de los tamaños de cada uno).
   BigInteger totient;
   BigInteger e, d;        //llaves privada y pública

 /** Constructor de la clase RSA */
   public RSA(int tamPrimo) {
      this.tamPrimo = tamPrimo;
      generaPrimos();             //Genera p y q
      generaClaves();             //Genera e y d
   }
 
   public void generaPrimos()
   {
      p = new BigInteger(tamPrimo, 10, new Random());
      do 
         q = new BigInteger(tamPrimo, 10, new Random());
      while(q.compareTo(p)==0);
   }
 
   public void generaClaves()
   {
     // n = p * q
      n = p.multiply(q);
     // toltient = (p-1)*(q-1)
      totient = p.subtract(BigInteger.valueOf(1));
      totient = totient.multiply(q.subtract(BigInteger.valueOf(1)));
     // Elegimos un e coprimo de y menor que n
      do e = new BigInteger(2 * tamPrimo, new Random());
         while((e.compareTo(totient) != -1) ||
      (e.gcd(totient).compareTo(BigInteger.valueOf(1)) != 0));
     // d = e^1 mod totient
      d = e.modInverse(totient);
      
   }
 
 /**
  * Cifra el texto usando la clave pública
  *
  * @param   mensaje     Ristra que contiene el mensaje a cifrar
  * @return   El mensaje cifrado como un BigInteger
       */
  
  
   public BigInteger cifrar(String mensaje, BigInteger e, BigInteger n)
   {
      BigInteger pt = new BigInteger(mensaje.getBytes());
      System.out.println("Mensaje númerico: "+pt);
               
      BigInteger ctNumber = pt.modPow(e,n);
     
      return ctNumber;
   }
 
 /**
  * Descifra el texto cifrado usando la clave privada
  *
  * @param   cifrado       BigInteger que contiene el texto cifrado
  *                           que será descifrado
  * @return  The decrypted String plaintext
  */
  
   public String descifrar(BigInteger ct, BigInteger d, BigInteger n) {
      BigInteger pt = ct.modPow(d,n);
      return(new String(pt.toByteArray()));
   }
   
   public BigInteger firmar(File f, BigInteger d, BigInteger n)throws Exception{
      SHA sha=new SHA("SHA-256");
      byte[] sha_file=sha.getHash(f);
      BigInteger pt = new BigInteger(sha_file);
      BigInteger ctNumber = pt.modPow(d,n);
      return ctNumber;
   }

   public Boolean check(File f,BigInteger ctNumber,  BigInteger e, BigInteger n)throws Exception{
      SHA sha=new SHA("SHA-256");
      byte[] sha_file=sha.getHash(f);
      BigInteger pt = new BigInteger(sha_file);
      BigInteger h =  ctNumber.modPow(e, n);
      if(h.equals(pt)){
         return true;
      }
      return false;
   }
          
   public BigInteger getp() {
      return(p);}
   public BigInteger getq() {
      return(q);}
   public BigInteger gettotient() {
      return(totient);}
   public BigInteger getn() {
      return(n);}
   public BigInteger gete() {
      return(e);}
   public BigInteger getd() {
      return(d);}
		
		
   public static void main(String args[]) throws Exception{
   	
      /*BufferedReader f = new BufferedReader(new InputStreamReader(System.in));
   	
      RSA rsa = new RSA(2048);
   	
      System.out.println("\n n = " + rsa.getn());
      System.out.println("\n e = " + rsa.gete());
      System.out.println("\n d = " + rsa.getd());
      
      System.out.println("Enter the plain text: ");
      String s = f.readLine();
   	
      System.out.println("Encryption started, please wait...");
      BigInteger cifradoNumber = rsa.cifrar(s,rsa.gete(),rsa.getn());
   
      System.out.println("Done!\n");
   	
              
      System.out.println("\n\nDecryption started, please wait...");
        
      String descifrado = rsa.descifrar(cifradoNumber, rsa.getd(), rsa.getn());
   	
      System.out.println("Done!\n");
   	
      System.out.println("Decrypted: " + descifrado);
      */

      File f= new File("/Users/drodriguez/proyectos/cinvestav/seg_informatica/cifradores_asimetricos/rsa/data.pdf");
      RSA rsa = new RSA(2048);

      BigInteger firmado = rsa.firmar(f, rsa.getd(), rsa.getn());

      Boolean pene=rsa.check(f, firmado, rsa.gete(), rsa.getn());
      System.out.println(pene);

                  
   }
}

class SHA {
    
    
   private MessageDigest digest;
   
   public SHA()
   {
       
   }
   
   public SHA(String SHAInstance) throws NoSuchAlgorithmException
   {
       setSHAInstance(SHAInstance);      
   }
   
   
   public void setSHAInstance(String shaType) throws NoSuchAlgorithmException
   {
       this.digest = MessageDigest.getInstance(shaType);
           
   }
   
   public byte[] getHash(File file) throws FileNotFoundException, IOException
   {
       InputStream fis = null;
       fis = new FileInputStream(file);
       int n = 0;
       byte[] buffer = new byte[8192];
       while (n != -1)
       {
           n = fis.read(buffer);
           if (n > 0) 
           {
               digest.update(buffer, 0, n);
           }
       }   
           
       return digest.digest();
       
   }
}


