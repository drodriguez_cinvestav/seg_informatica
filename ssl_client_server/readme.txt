** no additional setup is required, just run the programs **

1. run the server
2. run the client
3. observe that the client connects to the server through a secure socket (TLS-enable TCP communication between client & server)
4. as a result of SSL/TLS execution, client & server agree a ciphersuite
5. They communicate a single message, over an ecrypted channel (tunnel)
