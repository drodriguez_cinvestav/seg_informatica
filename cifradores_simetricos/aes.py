from Crypto import Random
from Crypto.Cipher import AES
###KDF###
from Crypto.Protocol.KDF import PBKDF2
from Crypto.Hash import SHA512
from Crypto.Random import get_random_bytes
###KDF###
from time import time
from os import listdir
import hashlib
import os

ruta="/Users/drodriguez/proyectos/cinvestav/seg_informatica/data/"

def pad(s):
    return s + b"\1" * (AES.block_size - len(s) % AES.block_size)

def cifrar(message, key):
    message = pad(message)
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    return iv + cipher.encrypt(message)

def decifrar(ciphertext, key):
    iv = ciphertext[:AES.block_size]
    cipher = AES.new(key, AES.MODE_CBC, iv)
    texto_plano = cipher.decrypt(ciphertext[AES.block_size:])
    return texto_plano.rstrip(b"\1")

def cifrar_archivo(archivo, key):
    ahora = time()
    with open(ruta+archivo, 'rb') as an:
        texto_plano = an.read()
    enc = cifrar(texto_plano, key)
    with open(ruta+'/cifrado/'+archivo+'.aes', 'wb') as an:
        an.write(enc)

    tiempo_empleado = time() - ahora
    tam_original=os.path.getsize(ruta+archivo)
    tam_cifrado=os.path.getsize(ruta+'/cifrado/'+archivo+'.aes')
    bps_cifrado=tam_original*8/tiempo_empleado
    print("{} ({} bytes) -> {} ({} bytes), {} segundos, {} bits/seg".format(archivo,tam_original,archivo+".aes",tam_cifrado,tiempo_empleado,bps_cifrado))

def decifrar_archivo(archivo, key):
    ahora = time()   
    with open(ruta+'cifrado/'+archivo+'.aes', 'rb') as ac:
        texto_cifrado = ac.read()
    dec = decifrar(texto_cifrado, key)

    with open(ruta+'descifrado/aes_'+archivo, 'wb') as ac:
        ac.write(dec)

    tiempo_empleado = time() - ahora
    tam_cifrado=os.path.getsize(ruta+'cifrado/'+archivo+'.aes')
    tam_descifrado=os.path.getsize(ruta+'descifrado/aes_'+archivo)
    bps_descifrado=tam_cifrado*8/tiempo_empleado
    print("{} ({} bytes) -> {} ({} bytes), {} segundos, {} bits/seg".format(archivo+'.aes',tam_cifrado,'aes_'+archivo,tam_descifrado,tiempo_empleado,bps_descifrado))

def getsha256(ruta,archivo):
    try:
        hashsha = hashlib.sha256()
        with open(ruta+archivo, "rb") as f:
            for bloque in iter(lambda: f.read(4096), b""):
                hashsha.update(bloque)
        return hashsha.hexdigest()
    except Exception as e:
        print("Error: %s" % (e))
        return ""
    except:
        print("Error desconocido")
        return ""

if __name__ == "__main__":
    archivos=listdir(ruta)
    archivos.remove('.DS_Store')
    archivos.remove('cifrado')
    archivos.remove('descifrado')
    print(archivos)

    kdf_on=False

    key = b'A2H3K4N5B6K7I6O7L0G9M9K3N5B6M7Z9'
    time_gen_key=0
    for archivo in archivos:
        if kdf_on:
            ahora = time()  
            password = b'my super secret'
            salt = get_random_bytes(16)
            key = PBKDF2(password, salt, 16, count=1000000, hmac_hash_module=SHA512)
            time_gen_key = time() - ahora

        print('\n\n************ Algoritmo AES: {} ***********'.format(archivo))
        print("KEY:{}, Tiempo:{}".format(key.hex(),time_gen_key))
        print('Cifrando: ##########')
        cifrar_archivo(archivo, key)
        print('Descifrando: ##########')
        decifrar_archivo(archivo, key)
        print('Check_Bits: ##########')
        print("SHA_Original:\t{}".format(getsha256(ruta,archivo)))
        print("SHA_Descifrado:\t{}".format(getsha256(ruta+'descifrado/','aes_'+archivo)))